# QiMap

Generate Keymap for a fast interface for interacting with a computer in a quick and efficient manner by using a keyboard / or stenographic device.

Have vi-like modal functionality (and even chorded keyboardS), insert special characters, have seperate clipboards.

## Keymapping design Principles

* Have a visual keyboard map with short descriptions (see qutebrowser)
  * have them also for when pressing modifiers.
* no default should be changed if it makes some sense in your usecase
* Less Keys
 * available / required
 * Do More (vim uses every key)
* Less Finger Movement
 * Less inputs required
* Shorthands

## Interoperability

Programs to interface with
* Classical UI / CTRL+C/CTRL+V
* xkbmap
* Vim mode bash
* neovim
* tmux
* a window manager
* desktop manager
* bash / emacs / readline insert mode

## Inputs

* Classical Keyboards.
* Laptop  Keyboards.
* Chorded Keyboards
* Stenography
* Audio
* translation


Journalists use little dashes and signs to transcribe human language fast, also in juries these
kind of stenograpic

## Healt of your Hand and Fingers

Make sure to use handcream to moisture your hands regularly.
Teke some breaks, think before you type, make sure you type
as little as possible.
Play guitar.
